'use strict';

document.addEventListener('DOMContentLoaded', function () {
    /* ------------ Main Carousel ------------ */
    const mainCarousel = new Swiper('.js-main-carousel', {
        loop: true,
        slidesPerView: 1,
        // spaceBetween: 10,

        pagination: {
            el: '.main-carousel__pagination',
            clickable: true
        },

        autoplay: {
            delay: 5000,
        },

        navigation: {
            nextEl: '.main-carousel__next',
            prevEl: '.main-carousel__prev',
        },

        breakpoints: {
            480: {},
            576: {},
            768: {},
            992: {},
            1200: {}
        }
    });
    /* ------------ Main Carousel ------------ */

    /* ------------ Opinions Carousel ------------ */
    const opinionsCarousel = new Swiper('.js-opinions-carousel', {
        loop: true,
        slidesPerView: 1,
        spaceBetween: 20,

        pagination: {
            el: '.opinions-carousel__pagination',
            clickable: true
        },

        navigation: false,

        breakpoints: {
            576: {
                slidesPerView: 2,
                spaceBetween: 30
            },
            768: {
                slidesPerView: 3
            }
        }
    });
    /* ------------ Opinions Carousel ------------ */

    /* ------------ Meating Carousel ------------ */
    const meatingsCarousel = new Swiper('.js-meating-carousel', {
        loop: true,
        slidesPerView: 2,

        navigation: {
            nextEl: '.meating-carousel__next',
            prevEl: '.meating-carousel__prev',
        },

        breakpoints: {
            480: {
                slidesPerView: 3
            },
            576: {
                slidesPerView: 4
            },
            768: {
                slidesPerView: 5
            },
            992: {
                slidesPerView: 6
            },
            1200: {
                slidesPerView: 7
            }
        }
    });
    /* ------------ Meating Carousel ------------ */

    /* ------------ Opinions Carousel ------------ */
    const trainingsCarousel = new Swiper('.js-trainings-carousel', {
        // loop: true,
        slidesPerView: 2,
        slidesPerColumn: 2,
        slidesPerColumnFill: 'row',
        // spaceBetween: 20,

        pagination: {
            el: '.trainings-carousel__pagination',
            clickable: true
        },

        navigation: false,

        breakpoints: {
            480: {},
            576: {
                slidesPerView: 3
            },
            768: {
                slidesPerView: 4
            },
            992: {
                slidesPerView: 5
            },
            //1200: {}
        }
    });
    /* ------------ Opinions Carousel ------------ */

    /* ------------ Choices Selects ------------ */
    const choices = new Choices('.js-select-default', {
        delimiter: ',',
        searchEnabled: false,
        itemSelectText: ''
    });

    /* Lang */
    if (window.matchMedia("(max-width: 767px)").matches) {
        let el = document.querySelector('.select-lang .choices__item--selectable');
        //el.innerHTML = el.dataset.value;

        document.querySelector('.select-lang .js-select-default').addEventListener('change', function (e) {
            let el = document.querySelector('.select-lang .choices__item--selectable');
            el.innerHTML = e.detail.value;
        });
    }
    /* Lang */

    /* Choices Phones */
    const regionsSelect = document.querySelector('.js-select-region');

    if (regionsSelect) {
        let worldwideChoices = new Choices(regionsSelect, {

            callbackOnCreateTemplates: function (strToEl) {

                let classNames = this.config.classNames;
                let itemSelectText = this.config.itemSelectText;

                return {

                    item: function (classNames, data) {
                        return strToEl(
                            '\
                <div\
                  class="' +
                            String(classNames.item) +
                            ' ' +
                            String(
                                data.highlighted
                                    ? classNames.highlightedState
                                    : classNames.itemSelectable,
                            ) +
                            '"\
                  data-item\
                  data-id="' +
                            String(data.id) +
                            '"\
                  data-value="' +
                            String(data.value) +
                            '"\
                  ' +
                            String(data.active ? 'aria-selected="true"' : '') +
                            '\
                  ' +
                            String(data.disabled ? 'aria-disabled="true"' : '') +
                            '\
                  >\
                  <span class="country-icon ' + data.value + '"></span> ' +
                            String(data.label) +
                            '\
                </div>\
              ',
                        );
                    },

                    choice: function (classNames, data) {
                        return strToEl(
                            '\
                  <div\
                    class="' +
                            String(classNames.item) +
                            ' ' +
                            String(classNames.itemChoice) +
                            ' ' +
                            String(
                                data.disabled
                                    ? classNames.itemDisabled
                                    : classNames.itemSelectable,
                            ) +
                            '"\
                    data-select-text="' +
                            String(itemSelectText) +
                            '"\
                    data-choice \
                    ' +
                            String(
                                data.disabled
                                    ? 'data-choice-disabled aria-disabled="true"'
                                    : 'data-choice-selectable',
                            ) +
                            '\
                    data-id="' +
                            String(data.id) +
                            '"\
                    data-value="' +
                            String(data.value) +
                            '"\
                    ' +
                            String(
                                data.groupId > 0 ? 'role="treeitem"' : 'role="option"',
                            ) +
                            '\
                    >\
                    <span class="flag-icon ' + data.value + '"></span> ' +
                            String(data.label) +
                            '\
                </div>\
              ',
                        );
                    },

                };

            },

            itemSelectText: '',
            searchEnabled: false

        });
    }
    /* Choices Phones */

    /* Choices Statuses */
    const statuses = document.querySelector('.js-select-statuses');

    if (statuses) {
        let statusChoice = new Choices(statuses, {
            delimiter: ',',
            searchEnabled: false,
            itemSelectText: '',

            callbackOnCreateTemplates: function (strToEl) {
                let classNames = this.config.classNames;
                let itemSelectText = this.config.itemSelectText;

                return {
                    choice: function (classNames, data) {
                        let placeholder = "";
                        if (data.placeholder) {
                            placeholder = classNames.placeholder;
                        }

                        return strToEl(
                            '\
                  <div\
                    class="' +
                            String(classNames.item) +
                            ' ' +
                            String(classNames.itemChoice) +
                            ' ' +
                            String(placeholder) +
                            ' ' +
                            String(
                                data.disabled
                                    ? classNames.itemDisabled
                                    : classNames.itemSelectable,
                            ) +
                            '"\
                    data-select-text="' +
                            String(itemSelectText) +
                            '"\
                    data-choice \
                    ' +
                            String(
                                data.disabled
                                    ? 'data-choice-disabled aria-disabled="true"'
                                    : 'data-choice-selectable',
                            ) +
                            '\
                    data-id="' +
                            String(data.id) +
                            '"\
                    data-value="' +
                            String(data.value) +
                            '"\
                    ' +
                            String(
                                data.groupId > 0 ? 'role="treeitem"' : 'role="option"',
                            ) +
                            '\
                    >\
                    <span class="item-' + data.id + '"></span> ' +
                            String(data.label) +
                            '\
                </div>\
              ',
                        );
                    },
                };
            }
        });
    }
    /* Choices Statuses */
    var carSelect;

    /* Choices Select Car */
    if (document.querySelector('.js-select-car')) {
        document.carSelect = new Choices('.js-select-car', {
            delimiter: ',',
            itemSelectText: '',
            searchResultLimit: 20
        });

        var carSelect = document.carSelect;
        /* Disable Selects */
        carSelect.forEach(function (item, i) {
            if (i > 1) {
                carSelect[i].disable();
            }
        });
        /* Disable Selects */

        /* Enable Selects */
        carSelect.forEach(function (item, i, arr) {
            let order = i + 1;

            if (carSelect[i].passedElement) {
                let elem = carSelect[i].passedElement.element;

                elem.addEventListener('change', function () {

                    if (arr[order] && arr[order].passedElement) {
                        let nextElem = arr[order].passedElement;

                        if (nextElem.isDisabled) {
                            arr[order].enable();
                            nextElem.enable();
                        }
                    }

                });
            }

        });
        /* Enable Selects */
    }
    /* Choices Select Car */

    /* Choices Select Truck */
    if (document.querySelector('.js-select-truck')) {
        let truckSelect = new Choices('.js-select-truck', {
            delimiter: ',',
            itemSelectText: '',
            searchResultLimit: 20
        });

        /* Disable Selects */
        truckSelect.forEach(function (item, i) {
            if (i > 1) {
                truckSelect[i].disable();
            }
        });
        /* Disable Selects */

        /* Enable Selects */
        truckSelect.forEach(function (item, i, arr) {
            let order = i + 1;

            if (truckSelect[i].passedElement) {
                let elem = truckSelect[i].passedElement.element;

                elem.addEventListener('change', function () {

                    if (arr[order] && arr[order].passedElement) {
                        console.log(arr[order].getValue(true));

                        let nextElem = arr[order].passedElement;

                        if (nextElem.isDisabled) {
                            arr[order].enable();
                            nextElem.enable();
                        }
                    }

                });
            }

        });
        /* Enable Selects */
    }
    /* Choices Select Truck */

    /* Autocomplete Type */
    if (document.querySelector('.js-select-autocomplete')) {
        let choicesAutocomplete = new Choices('.js-select-autocomplete', {
            delimiter: ',',
            itemSelectText: '',
            noResultsText: 'Ничего не найдено!'
        });
    }
    /* Autocomplete Type */
    /* ------------ Choices Selects ------------ */

    /* ------------ Footer Collapse ------------ */
    document.querySelector('.js-footer-collapse').addEventListener('click', function (e) {
        let target = e.target.closest('.footer-collapse__title');

        if (!target) return;

        target.parentNode.classList.toggle('is-toggled');
    });
    /* ------------ Footer Collapse ------------ */

    /* ------------ Top-Header-Nav ------------ */
    let mainNav = document.querySelector('.top-header-nav__list'),
        navDropdown = document.querySelector('.top-header-nav__dropdown');

    window.onresize = checkItemsWidth;
    if (navDropdown) {
        checkItemsWidth();
    }

    function checkItemsWidth() {
        if (!mainNav)
            return;
        const navDropdownChildren = (navDropdown != null) ? navDropdown.children : null,
            mainNavWidth = mainNav.offsetWidth,
            mainNavChlidren = mainNav.children;
        let totalWidth = 0;

        if (window.matchMedia('(max-width: 767px)').matches) {

            [...mainNavChlidren].forEach(item => {
                navDropdown.appendChild(item);
            });

        } else {
            if (navDropdownChildren != null) {
                [...navDropdownChildren].forEach(item => {
                    mainNav.appendChild(item);
                });
            }
            [...mainNavChlidren].forEach(item => {
                totalWidth += item.offsetWidth;

                if (totalWidth > mainNavWidth) {
                    navDropdown.appendChild(item);
                }
            });

        }

        /* If dropdonw not empty */
        (!!navDropdownChildren.length)
            ?
            mainNav.closest('nav').classList.add('has-dropdown')
            :
            mainNav.closest('nav').classList.remove('has-dropdown');
        /* If dropdonw not empty */
    }

    /* Events */
    if (document.querySelector('.js-dropdown-toggle')) {
        document.querySelector('.js-dropdown-toggle').addEventListener('click', function (e) {
            let navbar = this.closest('.top-header-nav');

            if (navbar.classList.contains('is-active')) {
                navbar.classList.remove('is-active');
                document.removeEventListener('click', clickOutside);
            } else {
                navbar.classList.add('is-active');
                document.addEventListener('click', clickOutside);
            }
        });
    }

    function clickOutside(e) {
        let target = e.target.closest('.top-header-nav');

        if (target) return;

        document.querySelector('.top-header-nav').classList.remove('is-active');
        document.removeEventListener('click', clickOutside);
    }
    /* Events */
    /* ------------ Top-Header-Nav ------------ */

    /* ------------ Toggle-Search-Form ------------ */
    document.querySelector('.search-form-toggle').addEventListener('click', function (e) {
        document.querySelector('.search-form').classList.toggle('is-opened');
    });
    /* ------------ Toggle-Search-Form ------------ */

    /* ------------ Catalog-Navbar ------------ */
    document.querySelector('.catalog-nav-toggle').addEventListener('click', function () {
        let body = document.body;

        if (!body.classList.contains('is-navbar-opened')) {
            body.classList.add('is-navbar-opened');
            body.style.paddingRight = scrollWidth() + 'px';
            addOverlay();

            setTimeout(function () {
                document.addEventListener('click', clickOutsideNavbar);
            });
        }
    });

    function clickOutsideNavbar(e) {
        let target = e.target.closest('.main-nav');

        if (target) return;

        document.body.classList.remove('is-navbar-opened');
        document.body.style.paddingRight = '';
        removeOverlay();
        document.removeEventListener('click', clickOutsideNavbar);
    }

    document.querySelector('.main-nav__close-nav-wrap').addEventListener('click', function (e) {
        document.body.classList.remove('is-navbar-opened');
        document.body.style.paddingRight = '';
        removeOverlay();
        document.removeEventListener('click', clickOutsideNavbar);
        e.preventDefault();
    });

    /* Toggles Inside */
    // create toggles
    let navbarLinks = document.querySelectorAll('.main-nav__container a');

    navbarLinks.forEach(function (item) {
        if (item.closest('li').querySelector('ul')) {
            item.classList.add('has-dropdown');
        }
    });

    let dropdownToggles = document.querySelectorAll('.main-nav .has-dropdown');

    dropdownToggles.forEach(function (item) {
        item.addEventListener('click', function (e) {
            item.closest('li').classList.toggle('is-opened');

            e.preventDefault();
        });
    });
    /* Toggles Inside */
    /* ------------ Catalog-Navbar ------------ */

    /* ------------ Modal-Win ------------ */
    /*let modalWinId;
  
    document.addEventListener('click', function(e) {
      let trigger = e.target.closest('[data-modal-win-trigger]');
  
      if ( !trigger ) return;
  
      modalWinId = trigger.dataset.modalWinTrigger;
  
      let body = document.body;
  
      if ( !body.classList.contains('is-modal-opened') ) {
        addOverlay();
        body.classList.add('is-modal-opened');
        body.style.paddingRight = scrollWidth() + 'px';
  
        document.querySelector('[data-modal-win="' + modalWinId + '"]').classList.add('is-visible');
  
        *//* Carousel Inside Modal *//*
    if (document.querySelector('[data-modal-win="' + modalWinId + '"]').querySelector('.swiper-container')) {
      initProductGallery(
        document.querySelector('[data-modal-win="' + modalWinId + '"] .product-main-promo__main .swiper-container'), 
        document.querySelector('[data-modal-win="' + modalWinId + '"] .product-main-promo__thumbs .swiper-container'),
        modalWinId
      );
    }
    *//* Carousel Inside Modal *//*

    setTimeout(function() {
      document.addEventListener('click', clickOutsideModalWin);
    });
  }

  e.preventDefault();
});

document.querySelectorAll('.modal-win__close').forEach(function(item) {
  item.addEventListener('click', function(e) {
    let target = e.target.closest('.modal-win__close');

    if ( !target ) return;

    removeOverlay();
    document.body.classList.remove('is-modal-opened');
    document.body.style.paddingRight = '';

    document.querySelector('[data-modal-win="' + modalWinId + '"]').classList.remove('is-visible');

    document.removeEventListener('click', clickOutsideModalWin);
  });
});

function clickOutsideModalWin(e) {
  let target = e.target.closest('.modal-win__body');

  if (target) return;

  removeOverlay();
  document.body.classList.remove('is-modal-opened');
  document.body.style.paddingRight = '';

  document.querySelector('[data-modal-win="' + modalWinId + '"]').classList.remove('is-visible');

  document.removeEventListener('click', clickOutsideModalWin);
}*/

    document.addEventListener('click', function (e) {
        let target = e.target.closest('[data-modal-win-trigger]');

        if (!target) return;

        let winId = target.dataset.modalWinTrigger,
            cls = '';

        if (winId === 'add') cls = 'modal-win__body--wide';

        let instance = new Modal({
            content: document.querySelector('[data-modal-win="' + winId + '"]'),
            className: cls
        });

        instance.open();

        e.preventDefault();
    });
    /* ------------ Modal-Win ------------ */

    /* ------------ Authorize-Dropdown ------------ */
    if (document.querySelector('.authorize__link--login')) {
        document.querySelector('.authorize__link--login').addEventListener('click', function (e) {
            let parent = this.closest('.authorize');

            if (!parent.classList.contains('is-dropdown-opened')) {
                parent.classList.add('is-dropdown-opened');

                setTimeout(function () {
                    document.addEventListener('click', clickOutsideAuthorize);
                });
            }

            e.preventDefault();
        });

        function clickOutsideAuthorize(e) {
            let target = e.target.closest('.authorize__dropdown');

            if (target) return;

            document.querySelector('.authorize').classList.remove('is-dropdown-opened');

            document.removeEventListener('click', clickOutsideAuthorize);
        }
    }
    /* ------------ Authorize-Dropdown ------------ */

    /* ------------ Soc Expanded ------------ */
    document.querySelectorAll('.soc__item--expand').forEach(function (item) {

        item.addEventListener('click', function (e) {
            this.closest('.soc').querySelectorAll('.soc__item').forEach(function (item) {
                item.style.display = '';
            });

            e.preventDefault();

            setTimeout(() => {
                this.remove();
            });
        });

    });
    /* ------------ Soc Expanded ------------ */

    /* ------------ Filter ------------ */
    /* Filter-Slider */
    let priceRange = document.getElementById('price-range');
    let inputNumberA = document.getElementById('input-number-a');
    let inputNumberB = document.getElementById('input-number-b');
    let inputs = [inputNumberA, inputNumberB];

    if (priceRange) {
        noUiSlider.create(priceRange, {
            start: [176000, 1000000],
            connect: true,
            // tooltips: true,
            range: {
                'min': 0,
                'max': 1000000
            },
            format: {
                to: function (e) {
                    return Math.round(e)
                },
                from: function (e) {
                    return Number(e)
                }
            }
        });

        priceRange.noUiSlider.on('update', function (values, handle) {
            inputs[handle].value = values[handle];
        });

        inputNumberA.addEventListener('change', function () {
            priceRange.noUiSlider.set([this.value, null]);
        });

        inputNumberB.addEventListener('change', function () {
            priceRange.noUiSlider.set([null, this.value]);
        });
    }
    /* Filter-Slider */

    /* Show more */
    let filterMoreToggle = document.querySelector('.filter__more-options');

    if (filterMoreToggle) {

        filterMoreToggle.addEventListener('click', function (e) {
            document.querySelectorAll('.filter__section').forEach(function (item) {
                item.classList.remove('is-hidden');
            });

            e.preventDefault();

            setTimeout(() => {
                this.remove();
            });
        });

    }
    /* Show more */

    /* Filter-Mobile */
    let filterToggle = document.querySelector('.filter-toggle');

    if (filterToggle) {

        filterToggle.addEventListener('click', function (e) {
            let body = document.body;

            if (!body.classList.contains('is-filter-opened')) {
                addOverlay();
                body.classList.add('is-filter-opened');
                body.style.paddingRight = scrollWidth() + 'px';

                setTimeout(function () {
                    document.addEventListener('click', clickOutsideFilter);
                });
            }

            e.preventDefault();

        });

    }

    let filterClose = document.querySelector('.filter__close');

    if (filterClose) {

        filterClose.addEventListener('click', function (e) {
            let target = e.target.closest('.filter');

            if (!target) return;

            removeOverlay();
            document.body.classList.remove('is-filter-opened');
            document.body.style.paddingRight = '';

            document.removeEventListener('click', clickOutsideFilter);
        });

    }

    function clickOutsideFilter(e) {
        let target = e.target.closest('.filter');

        if (target) return;

        removeOverlay();
        document.body.classList.remove('is-filter-opened');
        document.body.style.paddingRight = '';

        document.removeEventListener('click', clickOutsideFilter);
    }
    /* Filter-Mobile */

    /* Filter-Toggle-Section */
    document.addEventListener('click', function (e) {
        let target = e.target.closest('.filter__title');

        if (!target) return;

        target.closest('.filter__section').classList.toggle('is-opened');
    });
    /* Filter-Toggle-Section */
    /* ------------ Filter ------------ */

    /* ------------ Counter ------------ */
    document.querySelectorAll('.counter--add').forEach(function (item) {

        item.addEventListener('click', function (e) {
            let input = this.closest('.counter').querySelector('input[type="tel"]');
            let value = input.value;

            if (!parseInt(value) === value) return;

            input.value = parseInt(value) + 1;
            input.onchange();
            e.preventDefault();
        });

    });

    document.querySelectorAll('.counter--remove').forEach(function (item) {

        item.addEventListener('click', function (e) {
            let input = this.closest('.counter').querySelector('input[type="tel"]');
            let value = input.value;

            if (!parseInt(value) === value) return;

            if (value > 0) {
                input.value = parseInt(value) - 1;
            }
           input.onchange();
            e.preventDefault();
        });
    });
    /* ------------ Counter ------------ */

    /* ------------ Manual Toggle ------------ */
    document.querySelectorAll('.product-line__docs').forEach(function (item) {
        item.addEventListener('click', function (e) {
            this.closest('.product-line-wrap').classList.remove('is-analogs-opened');
            this.closest('.product-line-wrap').classList.toggle('is-manual-opened');

            e.preventDefault();
        });
    });

    document.querySelectorAll('.product-line__manual-close').forEach(function (item) {
        item.addEventListener('click', function (e) {
            if (this.closest('.product-line-wrap')) {
                this.closest('.product-line-wrap').classList.remove('is-analogs-opened');
                item.closest('.product-line-wrap').classList.toggle('is-manual-opened');
            }

            if (this.closest('tbody')) {
                this.closest('tbody').classList.remove('manual-section');
            }
        });
    });

    document.querySelectorAll('.product-line__analogs').forEach(function (item) {
        item.addEventListener('click', function (e) {
            this.closest('.product-line-wrap').classList.remove('is-manual-opened');
            this.closest('.product-line-wrap').classList.toggle('is-analogs-opened');

            e.preventDefault();
        });
    });

    document.querySelectorAll('.product-line__analogs-close').forEach(function (item) {
        item.addEventListener('click', function (e) {
            if (this.closest('.product-line-wrap')) {
                this.closest('.product-line-wrap').classList.remove('is-manual-opened');
                item.closest('.product-line-wrap').classList.toggle('is-analogs-opened');
            }

            if (this.closest('tbody')) {
                this.closest('tbody').classList.remove('analogs-section');
            }
        });
    });
    /* ------------ Manual Toggle ------------ */

    /* ------------ Product-Main ------------ */
    let galleryThumbs = new Swiper('.js-thumbs-list', {
        direction: 'vertical',
        slidesPerView: 5,
        spaceBetween: 5,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        height: "65px",
        navigation: {
            prevEl: '.product-main-promo__th-prev',
            nextEl: '.product-main-promo__th-next'
        }
    });

    let galleryMain = new Swiper('.js-promo-main', {
        spaceBetween: 10,
        // effect: 'coverflow',

        pagination: {
            el: '.product-main-promo__pagination',
            clickable: true
        },

        breakpoints: {
            768: {}
        },

        thumbs: {
            swiper: galleryThumbs
        }
    });

    lightGallery(document.querySelector('.js-promo-main'), {
        selector: ".product-main-promo__img-wrap"
    });

    lightGallery(document.querySelector('.js-lgallery'));

    lightGallery(document.querySelector('.meating-carousel'), {
        selector: ".meating-promo"
    });

    /* Drift Zoom */
    if (window.matchMedia('(min-width: 992px)').matches) {

        let driftPaneContainer = document.querySelector(".product-main-promo__zoom-pane");
        let driftInstance = null;

        initDriftZoom(document.querySelector(".js-promo-main .swiper-slide-active img"), driftPaneContainer);

        galleryMain.on('slideChange', function () {
            setTimeout(function () {

                driftInstance.destroy();
                initDriftZoom(document.querySelector(".js-promo-main .swiper-slide-active img"), driftPaneContainer);

            });
        });

        function initDriftZoom(driftTriggerElement, driftPaneContainer) {
            if (!driftTriggerElement) return;

            driftInstance = new Drift(driftTriggerElement, {
                paneContainer: driftPaneContainer,
                hoverBoundingBox: true,
                zoomFactor: 4,
                inlinePane: false,
                handleTouch: false
            });
        }

    }
    /* Drift Zoom */
    /* ------------ Product-Main ------------ */

    /* ------------ Items Carousels ------------ */
    let opts1 = {
        //loop: true,
        slidesPerView: 1,
        spaceBetween: 10,

        pagination: {
            el: '.default-pagination',
            clickable: true
        },

        navigation: {
            prevEl: '.sw-prev-1',
            nextEl: '.sw-next-1'
        },

        breakpoints: {
            480: {
                slidesPerView: 2
            },
            576: {
                slidesPerView: 3
            },
            768: {
                slidesPerView: 4
            },
            992: {
                slidesPerView: 5
            },
            /*1200: {
              slidesPerView: 6
            }*/
        }
    };

    let instanceOne = new Swiper('.js-instance-1', opts1);
    /* ------------ Items Carousels ------------ */

    /* ------------ Tabs ------------ */
    document.querySelectorAll('.tabs').forEach(function (item) {
        item.querySelectorAll('.tabs__link').forEach(function (item, i) {
            item.addEventListener('click', function (e) {
                let target = e.target;

                e.preventDefault();

                if (target.classList.contains('is-active')) return;
				if (target.dataset.handler) {
					executeFunctionByName(target.dataset.handler, window, target);
				}

                let hash = target.getAttribute('href');
                if (hash != '#') {
                    window.location.hash = hash;
                    window.location.assign(hash);
                }

                let root = target.closest('.tabs');
                clearTabClasses(root, i);
            });
        });
    });

    function clearTabClasses(root, i) {
        root.querySelectorAll('.tabs__item').forEach(function (item) {
            item.querySelector('.tabs__link').classList.remove('is-active');
        });
        root.querySelectorAll('.tabs__tab-pane').forEach(function (item) {
            item.classList.remove('is-active');
        });

        root.querySelector('.tabs__item:nth-child(' + (i + 1) + ') .tabs__link').classList.add('is-active');
        root.querySelector('.tabs__tab-pane:nth-child(' + (i + 1) + ')').classList.add('is-active');

        /* Update Carousel Inside Tabs */
        if (root.querySelector('.tabs__tab-pane:nth-child(' + (i + 1) + ')').querySelector('.items-carousel')) {
            instanceOne.update();
        }
        /* Update Carousel Inside Tabs */
    }

    let currentHash = window.location.hash;
    let hashTab = document.querySelector('.tabs__link[href="' + currentHash + '"]');
    if (hashTab) {
        let ev = new Event('click');
        hashTab.dispatchEvent(ev);
    }

    /* Tabs Accordeon */
    document.addEventListener('click', function (e) {
        let toggle = e.target.closest('.tabs__accordeon-toggle');

        if (!toggle) return;

        let parent = toggle.closest('.tabs__tab-pane');

        if (parent.classList.contains('is-opened')) {

            parent.classList.remove('is-opened');

        } else {

            document.querySelectorAll('.tabs__tab-pane').forEach(function (item) {
                item.classList.remove('is-opened');
            });

            parent.classList.add('is-opened');
			let target = e.target;
			if (target.dataset.handler) {
				executeFunctionByName(target.dataset.handler, window, target);
			}

            /* Update Carousel Inside Tabs */
            if (parent.querySelector('.items-carousel')) {
                instanceOne.update();
            }
            /* Update Carousel Inside Tabs */
        }
    });
    /* Tabs Accordeon */

    /* Go To Full Params Tab */
    let toggle = document.querySelector('.product-main__params-more');

    if (toggle) {
        toggle.addEventListener('click', function (e) {
            let hash = this.getAttribute('href');
            console.log(hash);
            let hashTab = document.querySelector('.tabs__link[href="' + hash + '"]');

            if (hashTab) {
                let ev = new Event('click');
                hashTab.dispatchEvent(ev);
            }

            e.preventDefault();
        });
    }
    /* Go To Full Params Tab */
    /* ------------ Tabs ------------ */

    /* ------------ Card-Table Collapse ------------ */
    document.querySelectorAll('.spec-table__collapse').forEach(function (item) {

        item.addEventListener('click', function (e) {
            document.querySelector('.card-specification-section').classList.toggle('is-collapsed');
        });

    });
    /* ------------ Card-Table Collapse ------------ */

    /* ------------ Speck-Heading Mob Collapse ------------ */
    document.querySelectorAll('.spec__heading').forEach(function (item) {

        item.addEventListener('click', function (e) {
            e.target.closest('.spec__product').classList.toggle('is-open');
        });

    });
    /* ------------ Speck-Heading Mob Collapse ------------ */

    /* ------------ Dealer Mob Collapse ------------ */
    document.querySelectorAll('[data-dealer-toggle]').forEach(function (toggle) {
        toggle.addEventListener('click', dataDealerToggleHandler);
    });

    /* ------------ Dealer Mob Collapse ------------ */

    /* ------------ Pick Date ------------ */
    let dateFrom = document.getElementById('date-from'),
        dateTo = document.getElementById('date-to');

    if (dateFrom) {
        let dateFromToggle = dateFrom.nextElementSibling;

        let dateFromPicker = new Pikaday({
            field: dateFrom,
            format: 'D/M/YYYY',
            bound: false,
            //container: document.querySelectorAll('.orders-filter__date-picker')[0],
            toString(date, format) {
                // you should do formatting based on the passed format,
                // but we will just return 'D/M/YYYY' for simplicity
                const day = date.getDate();
                const month = date.getMonth() + 1;
                const year = date.getFullYear();
                return `${day}/${month}/${year}`;
            },
            parse(dateString, format) {
                // dateString is the result of `toString` method
                const parts = dateString.split('/');
                const day = parseInt(parts[0], 10);
                const month = parseInt(parts[1], 10) - 1;
                const year = parseInt(parts[2], 10);
                return new Date(year, month, day);
            },
            firstDay: 1,
            i18n: {
                previousMonth: 'Следующий месяц',
                nextMonth: 'Предыдущий месяц',
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
            },
            onSelect: function (d) {
                this.hide();
            }
        });

        dateFromPicker.hide();

		if (dateFromToggle) {
			dateFromToggle.addEventListener('click', function (e) {
				if (dateFromPicker.isVisible()) {
					dateFromPicker.hide();
				} else {
					dateFromPicker.show();
				}
			});
		}
    }

    if (dateTo) {
        let dateToToggle = dateTo.nextElementSibling;

        let dateToPicker = new Pikaday({
            field: dateTo,
            format: 'D/M/YYYY',
            bound: false,
            //container: document.querySelectorAll('.orders-filter__date-picker')[1],
            toString(date, format) {
                // you should do formatting based on the passed format,
                // but we will just return 'D/M/YYYY' for simplicity
                const day = date.getDate();
                const month = date.getMonth() + 1;
                const year = date.getFullYear();
                return `${day}/${month}/${year}`;
            },
            parse(dateString, format) {
                // dateString is the result of `toString` method
                const parts = dateString.split('/');
                const day = parseInt(parts[0], 10);
                const month = parseInt(parts[1], 10) - 1;
                const year = parseInt(parts[2], 10);
                return new Date(year, month, day);
            },
            firstDay: 1,
            i18n: {
                previousMonth: 'Следующий месяц',
                nextMonth: 'Предыдущий месяц',
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                weekdaysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
            },
            onSelect: function (d) {
                this.hide();
            }
        });

        dateToPicker.hide();

        dateToToggle.addEventListener('click', function (e) {
            if (dateToPicker.isVisible()) {
                dateToPicker.hide();
            } else {
                dateToPicker.show();
            }
        });
    }
    /* ------------ Pick Date ------------ */

    /* ------------ Orders Filter ------------ */
    let ordersFilterToggle = document.querySelector('.orders-filter-toggle');

    if (ordersFilterToggle) {
        ordersFilterToggle.addEventListener('click', function (e) {
            let filter = document.querySelector('.orders-filter')
            filter.classList.toggle('is-opened');

            e.preventDefault();
        });
    }

    let ordersFilterClose = document.querySelector('.orders-filter__close');

    if (ordersFilterClose) {
        ordersFilterClose.addEventListener('click', function (e) {
            this.closest('.orders-filter').classList.remove('is-opened');
        });
    }
    /* ------------ Orders Filter ------------ */

    /* ------------ Orders Mob Toggle ------------ */
    document.querySelectorAll('.orders-mob__order-body-toggle').forEach(function (item) {
        item.addEventListener('click', function (e) {
            this.closest('.orders-mob__order').classList.toggle('is-opened');
        });
    });
    /* ------------ Orders Mob Toggle ------------ */

    /* ------------ Orders Mob Toggle ------------ */
    document.querySelectorAll('.orders-list__mob-toggle').forEach(function (item) {
        item.addEventListener('click', function (e) {
            this.closest('.orders-list__mob-order').classList.toggle('is-opened');
        });
    });
    /* ------------ Orders Mob Toggle ------------ */

    /* ------------ Tree Nav Sidebar Toggle ------------ */
    let treeNavSectionToggles = document.querySelectorAll('.tree-nav-section__header');

    treeNavSectionToggles.forEach(function (item) {

        item.addEventListener('click', function () {
            this.closest('.tree-nav-section').classList.toggle('is-opened');
        });

    });
    /* ------------ Tree Nav Sidebar Toggle ------------ */

    /* ------------ Tree Nav Menu ------------ */
    let treeNavs = document.querySelectorAll('.tree-nav');

    treeNavs.forEach(function (item) {
        item.querySelectorAll('li').forEach(function (item) {
            if (!item.querySelector('ul')) return;

            item.classList.add('has-children');
            item.prepend(createTreeNavToggle());
        });
    });

    document.querySelectorAll('li.has-children > .tree-nav__toggle').forEach(function (item) {
        item.addEventListener('click', function (e) {
            this.closest('li').classList.toggle('is-active');

            e.preventDefault();
        });
    });

    function createTreeNavToggle() {
        let span = document.createElement('span');
        span.classList = 'tree-nav__toggle';
        return span;
    }
    /* ------------ Tree Nav Menu ------------ */

    /* ------------ Accordeon ------------ */
    let headings = document.querySelectorAll('.accordeon__heading');

    headings.forEach(function (item) {

        item.addEventListener('click', function () {
            this.classList.toggle('is-active');
        });

    });
    /* ------------ Accordeon ------------ */

    /* ------------ Radio Opt States ------------ */
    let optStates = document.querySelectorAll('.radio-opt');

    optStates.forEach(function (item) {
        item.querySelector('input[type="radio"]').addEventListener('change', function () {
            clearOptStates(optStates);
            this.closest('.radio-opt').classList.add('active');
        });
    });

    function clearOptStates(opts) {
        opts.forEach(function (item) {
            item.classList.remove('active');
        });
    }
    /* ------------ Radio Opt States ------------ */

    /* ------------ Tippy JS ------------ */
    tippy('[data-tippy-content]', {
        theme: 'white-border'
    });

    tippy('[data-template]', {
        theme: 'white-border',
        interactive: true,
        // trigger: 'click',
        placement: 'bottom-end',
        arrow: false,
        // followCursor: true,
        maxWidth: 600,
        boundary: HTMLElement,
        appendTo: document.body,
        popperOptions: {
            positionFixed: true
        },
        content(reference) {
            const id = reference.getAttribute('data-template');
            const template = document.querySelector('[data-tooltip-content="' + id + '"]');

            if (template)
                return template.innerHTML;
        }
    });
    /* ------------ Tippy JS ------------ */

    /* ------------ Table inside Table ------------ */
    document.addEventListener('click', function (e) {
        let target = e.target.closest('.spec-payment-table__toggle');

        if (!target) return;

        target.closest('.spec-payment-table__tbody').classList.toggle('is-opened');
    });

    document.addEventListener('click', function (e) {
        let target = e.target.closest('.spec-payment-toggle');

        if (!target) return;

        let parents = document.querySelectorAll('#spec-payment-table > tbody');

        if (target.classList.contains('active')) {
            // clear active
            parents.forEach(function (item) {
                item.classList.remove('is-opened');
            });

            target.classList.remove('active');
            target.firstChild.data = 'Развернутый вид';
        } else {
            // add active
            parents.forEach(function (item) {
                item.classList.add('is-opened');
            });

            target.classList.add('active');
            target.firstChild.data = 'Свернутый вид';
        }

        e.preventDefault();
    });
    /* ------------ Table inside Table ------------ */

    /* ------------ More Roles ------------ */
    document.addEventListener('click', function (e) {
        let target = e.target;

        if (!target.classList.contains('js-more-roles')) return;

        target.parentNode.querySelectorAll('.user-roles__role').forEach(function (role) {
            if (role.classList.contains('is-hidden'))
                role.classList.remove('is-hidden');
        });

        target.hidden = true;

        e.preventDefault();
    });
    /* ------------ More Roles ------------ */

    /* ------------ Video FROM YOUTUBE!!! ------------ */
    function init() {
        var vidDefer = document.getElementsByTagName('iframe');
        for (var i = 0; i < vidDefer.length; i++) {
            if (vidDefer[i].getAttribute('data-src')) {
                vidDefer[i].setAttribute('src', vidDefer[i].getAttribute('data-src'));
            }
        }
    }

    window.onload = init;
    /* ------------ Video FROM YOUTUBE!!! ------------ */

    /* ------------ Custom Scroll ------------ */
    let filterBodies = document.querySelectorAll('.filter__body:not(.filter__body--free-space)');

    if (filterBodies) {
        filterBodies.forEach(function (el) {
            new SimpleBar(el, {
                autoHide: false,
                scrollbarMinSize: 4
            });
        });
    }

    let compareBodies = document.querySelectorAll('.compare__main');

    if (compareBodies) {
        compareBodies.forEach(function (el) {
            new SimpleBar(el, {
                autoHide: false,
                scrollbarMinSize: 4
            });
        });
    }

    /*Array.prototype.forEach.call(
      document.querySelectorAll('.filter__body'),
      el => new SimpleBar()
    );*/
    /* ------------ Custom Scroll ------------ */

    /* ------------ Switch reg user type ------------ */
    const userRadioTypes = document.querySelectorAll('[name="user-role"]');

    if (userRadioTypes) {
        userRadioTypes.forEach(function (role) {
            role.addEventListener('change', function (e) {
                let role = this.value;
                let fields = document.querySelector('.extra-fields');

                if (role == 'user-role-1') {
                    fields.classList.add('is-hidden');
                } else {
                    fields.classList.remove('is-hidden');
                }
            });
        });
    }
    /* ------------ Switch reg user type ------------ */

    /* ------------ Close-Notification ------------ */
    document.addEventListener('click', function (e) {
        let target = e.target.closest('.alert-notification__close');

        if (!target) return;

        target.closest('.alert-notification').remove();
    });
    /* ------------ Close-Notification ------------ */

    /* BOTTOM INPUT TYPE FILE */
    const fileInputs = document.querySelectorAll('input[type="file"]');

    Array.prototype.forEach.call(fileInputs, function (input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener('change', function (e) {
            var fileName = '';

            if (this.files && this.files.length > 1) {
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            } else {
                fileName = e.target.value.split("\\").pop();
            }

            if (fileName) {
                label.querySelector('span').innerHTML = fileName;
            } else {
                label.innerHTML = labelVal;
            }
        });
    });
    /* BOTTOM INPUT TYPE FILE */

    /* ------------------------- ADD TO COMPARE -------------------------- */
    document.addEventListener('click', function (e) {
        let target = e.target.closest('.product__compare');

        if (!target) return;

        let instance = new Modal({
            content: document.querySelector('.compare-success')
        });

        instance.open();

        e.preventDefault();
    });
    /* ------------------------- ADD TO COMPARE -------------------------- */

    /* Toggle Show More Params */
    const paramsToggle = document.querySelector('.params__btn');

    if (paramsToggle) {

        paramsToggle.addEventListener('click', function (e) {
            const paramsWrapper = document.querySelector('.params__main-wrap');

            if (paramsWrapper.classList.contains('active')) {
                paramsWrapper.classList.remove('active');
                this.firstChild.data = "Показать все";
            } else {
                paramsWrapper.classList.add('active');
                this.firstChild.data = "Скрыть";
            }

            e.preventDefault();
        });

    }
    /* Toggle Show More Params */

    /* --- Stock Table - specs and analogs --- */
    if (document.querySelector('.stock-table__docs')) {
        document.querySelectorAll('.stock-table__docs').forEach((link) => {
            link.addEventListener('click', stockTableDocsClickHandler);
        });
    }

    if (document.querySelector('.stock-table__analogs')) {
        document.querySelectorAll('.stock-table__analogs').forEach((link) => {
            link.addEventListener('click', function (e) {
                let tr = this.closest('tr');
                let tbody = tr.closest('tbody');

                tbody.classList.remove('manual-section');

                if (tbody.classList.contains('analogs-section')) {
                    tbody.classList.remove('analogs-section');
                } else {
                    tbody.classList.add('analogs-section');
                }

                e.preventDefault();
            });
        });
    }
    /* --- Stock Table - specs and analogs --- */

    /* ------------ Sort-By Toggle Mobile ------------ */
    const sortByToggle = document.querySelector('.js-sort-by-toggle');

    if (sortByToggle) {
        sortByToggle.addEventListener('click', function (e) {
            let body = document.body;

            if (!body.classList.contains('is-sort-buy-opened')) {
                addOverlay();
                body.classList.add('is-sort-buy-opened');
                body.style.paddingRight = scrollWidth() + 'px';

                setTimeout(function () {
                    document.addEventListener('click', clickOutsideSortBy);
                });
            }

            e.preventDefault();
        });
    }

    const sortByClose = document.querySelector('.sort-by-window__close');

    if (sortByClose) {

        sortByClose.addEventListener('click', function (e) {
            let target = e.target.closest('.sort-by-window');

            if (!target) return;

            removeOverlay();
            document.body.classList.remove('is-sort-buy-opened');
            document.body.style.paddingRight = '';

            document.removeEventListener('click', clickOutsideSortBy);
        });

    }

    function clickOutsideSortBy(e) {
        let target = e.target.closest('.sort-by-window');

        if (target) return;

        removeOverlay();
        document.body.classList.remove('is-sort-buy-opened');
        document.body.style.paddingRight = '';

        document.removeEventListener('click', clickOutsideSortBy);
    }
    /* ------------ Sort-By Toggle Mobile ------------ */

    /* --- Banner --- */
    if (document.querySelector('.header-banner__close')) {
        const bannerToggles = document.querySelectorAll('.header-banner__close');

        bannerToggles.forEach((bannerToggle) => {
            bannerToggle.addEventListener('click', function (e) {
                e.target.closest('.header-banner').hidden = true;

                e.preventDefault();
            });
        });
    }
    /* --- Banner --- */

    /* --- Toggle Action Details --- */
    if ( document.querySelector('.action__details-toggle') ) {
        const actionDetailToggles = document.querySelectorAll('.action__details-toggle');

        actionDetailToggles.forEach((toggle) => {
            toggle.addEventListener('click', (e) => {
                e.preventDefault();

                const toggle = e.target;
                const parent = toggle.closest('.action');

                if ( parent.classList.contains('opened') ) {
                    parent.classList.remove('opened');
                    //toggle.firstChild.data = 'Подробности ';
                } else {
                    parent.classList.add('opened');
                    //toggle.firstChild.data = 'Скрыть ';
                }
            });
        });
    }
    /* --- Toggle Action Details --- */

    /* --- Actions Mobile --- */
    const actionsToggle = document.querySelector('.js-actions-toggle');

    if (actionsToggle) {
        actionsToggle.addEventListener('click', function() {
            document.body.classList.toggle('is-actions-opened');
        });
    }
    /* --- /Actions Mobile --- */
});




















































svg4everybody({});


/* Utilities */
function scrollWidth() {
    let div = document.createElement('div');
    div.style.overflowY = 'scroll';
    div.style.width = '50px';
    div.style.height = '50px';
    div.style.visibility = 'hidden';
    document.body.appendChild(div);
    let scrollWidth = div.offsetWidth - div.clientWidth;
    document.body.removeChild(div);
    return scrollWidth;
}

function addOverlay() {
    let overlay = document.createElement('div');
    overlay.className = 'overlay';
    document.body.appendChild(overlay);
}

function removeOverlay() {
    let overlay = document.querySelector('.overlay')

    if (overlay) {
        overlay.remove();
    }
}

/*!
 * Get all direct descendant elements that match a selector
 * Dependency: the matches() polyfill: https://vanillajstoolkit.com/polyfills/matches/
 * (c) 2018 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param  {Node}   elem     The element to get direct descendants for
 * @param  {String} selector The selector to match against
 * @return {Array}           The matching direct descendants
 */
function childrenMatches(elem, selector) {
    return Array.prototype.filter.call(elem.children, function (child) {
        return child.matches(selector);
    });
};

function dataDealerToggleHandler(e) {
    const parent = e.target.closest('.spec__product');
    const toggleName = this.dataset.dealerToggle;
    const dropInfoBlock = childrenMatches(parent, "[data-dealer='" + toggleName + "']")[0]; //parent.querySelector("[data-dealer='" + toggleName + "']");

    if (dropInfoBlock) {
        parent.querySelectorAll("[data-dealer]").forEach((item) => {
            if (!(item.dataset.dealer === toggleName)) {
                item.classList.remove('active');
            }
        });

        dropInfoBlock.classList.toggle('active');
    }

    e.preventDefault();
}

function stockTableDocsClickHandler(e) {
    let tr = this.closest('tr');
    let tbody = tr.closest('tbody');

    tbody.classList.remove('analogs-section');

    if (tbody.classList.contains('manual-section')) {
        tbody.classList.remove('manual-section');
    } else {
        tbody.classList.add('manual-section');
    }

    e.preventDefault();
}

function executeFunctionByName(functionName, context) {
    var args = Array.prototype.slice.call(arguments, 2);
    var namespaces = functionName.split(".");
    var func = namespaces.pop();
    for (var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
    }
    return context[func].apply(context, args);
}